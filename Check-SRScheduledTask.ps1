<#
.SYNOPSIS
    Skrypt sprawdza stan wykonania zadań w TaskSchedulerze.

.DESCRIPTION
    Skrypt łączy się z podanymi serwerami i sprawdza stan wykonania tasków,
    wyświetlając informacje: Nazwa komputera, ścieżka do zadania, nazwa zadania, czas ostatniego uruchomienia, rezultat ostatniego uruchomienia.
    Jeśli task nie wykonał się w podanym czasie lub wykonał się, ale kod ostatniego wykonania świadczy o problemie będzie to zaznaczone na wyjściu
    kolorem czerwonym. 
  
.PARAMETER ServerName
    Parametr określający nazwę serwera lub kilku serwerów. Parametr wymagany.

.PARAMETER TaskPath
    Parametr określający ścieżkę do zadania w TaskScheluderze. Parametr wymagany.

.PARAMETER Log
    Ścieżka do katalogu w którym zapisany będzie log z wykonania skryptu. Parametr opcionalny.

.PARAMETER Days
    Parametr określający co ile dni powinien wykonywać się task. (Jeśli co dziennie, ustawiamy na 1, jeśli co drugi dzień ustawiamy na 2, itd...). 
    Parametr wymagany.

.INPUTS
    None

.OUTPUTS
    Plik logu (Check-SRScheduledTask_2024-02-22_18.13.36.log) w formacie:

    2024-02-22_18.13.36[INFO] > Log started
    2024-02-22_18.13.39 [INFO] > Sgodb03         \EPO_K3\                  epo_ekn                                22.02.2024 15:15:15          0
    2024-02-22_18.13.39 [INFO] > Sgodb03         \EPO_K3\                  epo_pobierzbraki                       21.02.2024 20:00:00          0
    2024-02-22_18.13.39 [INFO] > Sgodb03         \Backup_baz\              Bazy                                   21.02.2024 22:00:00          0
    2024-02-22_18.13.39 [INFO] > Sgodb03         \EPO_K3\                  epo_pobierzslowniki                    21.02.2024 22:00:00          0
    2024-02-22_18.13.39 [ERROR] > Sgops01         \backup_NeurocourtIM\     backup_NIM                             01.02.2024 21:00:00          0
    ...

.NOTES
  Version:        1.1
  Author:         Sebastian Cichonski
  Creation Date:  12.2023
  Projecturi:     https://gitlab.com/Sebastian_Cichonski/check-srscheduledtask
  
.EXAMPLE
    Check-SRScheduledTask.ps1 -ServerName Sgodb03, Sgops01 -TaskPath \backup_NeurocourtIM\, \backup_baz\, \ekspoer_wokand\, 
    \EPO_k3\, \epo_s2\, \scw_k3\, \scw_s2\, \pobierz_dane_PI\ -Days 1

ComputerName         TaskPath                  TaskName                                       LastRunTime LastTaskResult
------------         --------                  --------                                       ----------- --------------
Sgodb03         \SCW_K3\                  PobierzDane                            22.02.2024 14:30:30          0
Sgodb03         \Backup_baz\              Bazy                                   21.02.2024 22:00:00          0
Sgodb03         \SCW_S2\                  PobierzBraki                           22.02.2024 04:00:00          0
Sgodb03         \EPO_S2\                  epo_ekn                                22.02.2024 15:15:15          0
Sgodb03         \EPO_K3\                  epo_ekn                                22.02.2024 15:15:15          0
Sgodb03         \EPO_S2\                  epo_pobierzbraki                       21.02.2024 20:00:00          0
Sgodb03         \EPO_K3\                  epo_pobierz_dane                       22.02.2024 18:00:00          0
Sgodb03         \EPO_S2\                  epo_pobierz_dane                       22.02.2024 18:00:00          0
Sgodb03         \EPO_S2\                  epo_pobierzslowniki                    21.02.2024 22:00:00          0
Sgodb03         \SCW_K3\                  PobierzBraki                           22.02.2024 03:00:00          0
Sgodb03         \EPO_K3\                  epo_pobierzslowniki                    21.02.2024 22:00:00          0
Sgodb03         \SCW_K3\                  WyślijDane                             22.02.2024 16:15:15          0
Sgodb03         \Pobierz_Dane_PI\         PobierzBraki                           21.02.2024 20:00:00          0
Sgodb03         \EPO_K3\                  epo_pobierzbraki                       21.02.2024 20:00:00          0
Sgodb03         \Pobierz_Dane_PI\         PobierzDane_PI                         22.02.2024 16:30:30          0
Sgodb03         \SCW_S2\                  WyślijDane                             22.02.2024 16:00:00          0
Sgodb03         \SCW_S2\                  PobierzDane                            22.02.2024 18:00:00          0
Sgops01         \backup_NeurocourtIM\     backup_NIM                             20.02.2024 21:00:00          0
#>


[CmdletBinding()]
Param(
    [Parameter(Mandatory)]
    [string[]]$ServerName,   

    [Parameter(Mandatory)]
    [string[]]$TaskPath,

    [Parameter(Mandatory)]
    #[alias ("Days")]
    [int16]$Days,
    
    #[parameter]
    [alias("Log")]
    [ValidateScript({Test-Path $_ -PathType "Container"})]
    $LogPath
)

function Get-LongDate {
    Get-Date -Format "yyyy-MM-dd_HH.mm.ss"
}
function Write-Log {
    param (
        [Parameter()]
        [string]$Type,
        [string]$Value
    )
    if($Type -eq "Err") {
        Write-Host -ForegroundColor Red "$Value"
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "$(Get-LongDate) [ERROR] > $Value"
        }
    }
    if($Type -eq "Info") {
        Write-Host "$Value"
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "$(Get-LongDate) [INFO] > $Value"
        }
    }
}

if($LogPath) {
    $LogDate = Get-LongDate
    $Log = "$LogPath\Check-SRScheduledTask_$LogDate.log"
    if(Test-Path -Path $Log) {
        Clear-Content -Path $Log
    }
    Add-Content -Path $Log -Encoding Ascii -Value "$(Get-LongDate)[INFO] > Log started"
}

$Tasks = Invoke-Command -ComputerName $ServerName -ScriptBlock {  Get-ScheduledTask -TaskPath $using:TaskPath -ErrorAction SilentlyContinue | Get-ScheduledTaskInfo  } 

$ToDay = Get-Date
$EdgeDate = $ToDay.AddDays(-$Days)
Write-Host ("{0,-20} {1,-25} {2,-32} {3,25} {4,10}" -f "ComputerName", "TaskPath", "TaskName", "LastRunTime", "LastTaskResult")
Write-Host ("{0,-20} {1,-25} {2,-32} {3,25} {4,10}" -f "------------", "--------", "--------", "-----------", "--------------")


foreach($Task in $Tasks) {
    $Output = ("{0,-20} {1,-25} {2,-32} {3,25} {4,10}" -f $Task.PSComputerName, $Task.TaskPath, $Task.TaskName, $Task.LastRunTime, $Task.LastTaskResult)
   
    $Difference = $Task.LastRunTime - $EdgeDate

    if(($Task.LastTaskResult -ne 0) -or ($Difference -lt $Days)) {
        Write-Log -Type "Err" -Value $Output
    }
    else {
        Write-Log -Type "Info" -Value $Output
    }
}